let score = document.querySelector(".score");
let startScreen = document.querySelector(".startScreen");
let gameArea = document.querySelector(".gameArea");
let gameMessage = document.querySelector(".gameMessage");



gameMessage.addEventListener('click', start);
startScreen.addEventListener('click', start);
document.addEventListener('keydown', pressOn);
document.addEventListener("keyup", pressOff);
let keys = {};
let player = {};

function start() {
    console.log("start");
    player.speed = 4;
    player.score = 0;
    player.inplay = true;
    gameArea.innerHTML = ""; //clearing out gameArea incase of RESTART

    gameMessage.classList.add("hide");
    startScreen.classList.add("hide");
    let superman = document.createElement("div");
    superman.setAttribute("class", "superman");
    let cape = document.createElement("span");
    cape.setAttribute("class", "cape");
    cape.pos = 15;
    cape.style.top = cape.pos + "px";
    superman.appendChild(cape);
    gameArea.appendChild(superman);
    player.x = superman.offsetLeft;
    player.y = superman.offsetTop;
   
    player.tower = 0;
    let spacing = 350;
    let gap = Math.floor((gameArea.offsetWidth) / spacing);
    // console.log(gap);
    for (let x = 0; x < gap; x++) {
        buildTowers(player.tower * spacing);
    }
    window.requestAnimationFrame(playGame);

}

function buildTowers(startPos) {
    let totalHeight = gameArea.offsetHeight;
    let totalWidth = gameArea.offsetWidth;
    player.tower++;
    let tower1 = document.createElement("div");
    tower1.start = startPos + totalWidth;
    tower1.classList.add("tower");
    tower1.innerHTML = "<br>" + player.tower;
    tower1.height = Math.floor(Math.random() * 350);
    tower1.style.height = tower1.height + "px";
    tower1.style.left = tower1.start + "px";
    tower1.style.top = "0px";
    tower1.x = tower1.start;
    tower1.id = player.tower;
    tower1.style.backgroundColor = "#160E1B";
    gameArea.appendChild(tower1);
    let towerSpace = Math.floor(Math.random() * 350) + 350;
    let tower2 = document.createElement("div");
    tower2.start = tower1.start;
    tower2.classList.add("tower");
    tower2.innerHTML = "<br>" + player.tower;
    tower2.style.height = totalHeight - tower1.height - towerSpace + "px";
    tower2.style.left = tower1.start + "px";
    tower2.style.bottom = "0px";
    tower2.x = tower1.start;
    tower2.id = player.tower;
    tower2.style.backgroundColor = "red";
    gameArea.appendChild(tower2);
}


function moveTowers(superman) {
    let lines = document.querySelectorAll(".tower");
    let counter = 0; //counts towers to remove
    lines.forEach(function (item) {
        // console.log(item);
        item.x -= player.speed;
        item.style.left = item.x + 'px';
        if (item.x < 0) {
            item.parentElement.removeChild(item);
            counter++;
        }
        if (colide(item, superman)) {
            playGameOver(superman);
        }
    })
    counter = counter / 2;
    for (let x = 0; x < counter; x++) {
        buildTowers(0);
    }
}


function colide(a, b) {
    let aRect = a.getBoundingClientRect();
    let bRect = b.getBoundingClientRect();

    if ((aRect.bottom < bRect.top) ||
        (aRect.top > bRect.bottom) ||
        (aRect.right < bRect.left) ||
        (aRect.left > bRect.right)) {
        return false;
    }
    return true
}




function playGame() {
    window.requestAnimationFrame(playGame);
    if (player.inplay) {
        let superman = document.querySelector(".superman")
        let cape = document.querySelector(".cape");
        let move = false;

        moveTowers(superman);

        if (keys.ArrowLeft && player.x > 0) {
            player.x -= player.speed;
            move = true;
        }
        if (keys.ArrowRight && player.x < (gameArea.offsetWidth - 50)) {
            player.x += player.speed;
            move = true;
        }
        if (keys.ArrowUp && player.y > 0) {
            player.y -= (player.speed * 2);
            move = true;
        }
        if (keys.ArrowDown && player.y < (gameArea.offsetHeight - 50)) {
            player.y += player.speed;
            move = true;
        }
        if (move) {
            cape.pos = (cape.pos == 15) ? 25 : 15;
            cape.style.top = cape.pos + "px";
        }

        player.y += (player.speed * 1);
        if (player.y > gameArea.offsetHeight) {
            console.log("game over");
            playGameOver(superman);
        }

        // player.y +=player.speed;
        superman.style.top = player.y + "px";
        superman.style.left = player.x + "px";
        player.score++;
        score.innerText = "Score:" + player.score;
    }

}

function playGameOver(superman) {
    player.inplay = false;
    gameMessage.classList.remove("hide");
    superman.setAttribute("style", "transform:rotate(190deg)");
    gameMessage.innerHTML = "Game Over<br>You scored " + player.score + "<br>";
    let btnRestart= document.createElement('button');
    btnRestart.textContent="Click here to start again!";
    btnRestart.setAttribute("style", "background-color: red; color: white;");
    gameMessage.appendChild(btnRestart);
    btnRestart.addEventListener('click', function(){
        window.location.reload();
    });

}

function pressOn(e) {
    // e.preventDefault();
    keys[e.code] = true;
    // console.log(keys);
}

function pressOff(e) {
    // e.preventDefault();
    keys[e.code] = false;
    // console.log(keys);
}