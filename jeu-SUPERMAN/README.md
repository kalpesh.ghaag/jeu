https://gitlab.com/simplonlyon/P9/projets/js-game/blob/master/README.md


PURPOSE OF THE GAME
----------------------
To understand and get familiar with JS, especially its functions, loops and event listners.


A DAY IN THE LIFE OF SUPERMAN
--------------------------------

"A day in the life of SUPERMAN" is a multilevel desktop game. This game will permit you to cross obstacles and experience a life of Superman. It has added background music to compliment gaming experience.
   

RECOMMENDED STEPS
-------------------

* Press F11 to have a better game experience.

Playing GAME
----------------

Use Arrow Right, Arrow Left, Arrow Top, Arrow Bottom to assist SUPERMAN to make his moves.

   
BUILT WITH
-------------
   *HTML
   *CSS
   *JS   


FOR DEVELOPERS AND CONTRIBUTORS
----------------------------------  
Clone the source locally:

$ git clone git@gitlab.com:kalpesh.ghaag/jeu.git

  
Code decoded
--------------
<img src="js-towerSpacing.png" alt="js.towerSpacing">
<img src="js-moveTowers.png" alt="js-moveTowers">
<img src="css-code.png" alt="css-code">

NEXT VERSION
--------------
Purpose is to add game levels in the following version.


Versioning
--------------
1.0.0

Author
--------

Kalpesh Ghag

Licence
-----------

This project is an opensource project, it has licensed under the ISC norms.


